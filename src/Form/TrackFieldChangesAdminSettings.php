<?php

/**
 * @file
 * Contains \Drupal\track_field_changes\Form\TrackFieldChangesAdminSettings.
 */

namespace Drupal\track_field_changes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TrackFieldChangesAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'track_field_changes_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['track_field_changes.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    // When displaying the page, make sure the list of fields is up-to-date.
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service */
    $bundle_info_service = \Drupal::service('entity_type.bundle.info');
    $bundle_info_service->clearCachedBundles();

    /** @var \Drupal\Core\Entity\EntityFieldManager $field_manager */
    $field_manager = \Drupal::service('entity_field.manager');

    $config = $this->config('track_field_changes.settings');
    $form['settings'] = [
      '#type' => 'vertical_tabs',
    ];
    $entities = \Drupal::entityTypeManager()->getDefinitions();
    uasort($entities, function($a, $b) {
      return strcmp($a->getLabel(), $b->getLabel());
    });

    foreach ($entities as $entity_type_id => $entity_type) {
      $form[$entity_type_id] = [
        '#type' => 'details',
        '#title' => $entity_type->getLabel(),
        '#group' => 'settings',
        '#tree' => TRUE,
      ];
      $bundles = $bundle_info_service->getBundleInfo($entity_type_id);

      $bundle_types = [];
      foreach ($bundles as $bundle_type_id => $bundle_type) {
        $bundle_types[$bundle_type_id] = $bundle_type['label'];
      }

      $form[$entity_type_id]['bundle_types'] = [
        '#type' => 'checkboxes',
        '#title' => t('Enable field audit on bundles'),
        '#description' => t('The bundles that should be audited.'),
        '#default_value' => array_keys($config->get($entity_type_id . '.bundle_types')),
        '#options' => $bundle_types,
      ];

      foreach ($config->get($entity_type_id . '.bundle_types') as $bundle_type_id) {
        if (!isset($bundles[$bundle_type_id])) {
          continue;
        }

        $options = $config->get($entity_type_id . '.' . $bundle_type_id) ?: [];

        $bundle = $bundles[$bundle_type_id];

        $form[$entity_type_id][$bundle_type_id] = [
          '#type' => 'details',
          '#title' => $bundle['label'],
        ];

        $form[$entity_type_id][$bundle_type_id]['disable_multiple'] = array(
          '#type' => 'checkbox',
          '#title' => t('Disable multiple records per entity?'),
          '#description' => t('Only store one record per entity in the database. Useful to prevent views results duplication.<br>Note that if were already tracking field changes before checking this box, you will need to remove the duplicate rows manually from the database.'),
          '#default_value' => $options['disable_multiple'],
        );
        $form[$entity_type_id][$bundle_type_id]['enable_log'] = array(
          '#type' => 'checkbox',
          '#title' => t('Enable Log'),
          '#description' => t('Enable log message.'),
          '#default_value' => $options['enable_log'],
        );
        $form[$entity_type_id][$bundle_type_id]['basic_new'] = array(
          '#type' => 'checkbox',
          '#title' => t('Basic audit for created entity'),
          '#description' => t('Record basic information (timestamp, user and log) when an entity is created.'),
          '#default_value' => $options['basic_new'],
        );
        $form[$entity_type_id][$bundle_type_id]['basic_revision'] = array(
          '#type' => 'checkbox',
          '#title' => t('Basic audit for updated entity'),
          '#description' => t('Record basic information (timestamp, user and log) when an entity is updated.'),
          '#default_value' => $options['basic_revision'],
        );
        $form[$entity_type_id][$bundle_type_id]['fields_audit'] = array(
          '#type' => 'checkbox',
          '#title' => t('Track field changes for updated entity'),
          '#description' => t('Enable fields audit on updated entities. Each selected and amended field will be recorded.'),
          '#default_value' => $options['fields_audit'],
        );

        $fields = $field_manager->getFieldDefinitions($entity_type_id, $bundle_type_id);

        $opts = [];
        foreach ($fields as $field_id => $field) {
          $opts[$field_id] = $field->getLabel() . ' (' . $field_id .')';
        }
        asort($opts);

        $form[$entity_type_id][$bundle_type_id]['fields'] = [
          '#type' => 'checkboxes',
          '#title' => t('Enable field audit'),
          '#description' => t('Select which fields need to be audited.'),
          '#default_value' => $options['fields'],
          '#options' => $opts,
          '#states' => [
            'visible' => [
              ':input[name="' . $entity_type_id . '[' . $bundle_type_id . '][fields_audit]"]' => [
                'checked' => TRUE
              ]
            ]
          ],
        ];

      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $config = $this->config('track_field_changes.settings');

    foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
      $vals = $form_state->getValue($entity_type_id);
      $vals['bundle_types'] = array_filter($vals['bundle_types']);
      if (empty($vals['bundle_types'])) {
        $config->clear($entity_type_id);
      }
      else {
        foreach ($vals as $bundle_type_id => $opts) {
          if ($bundle_type_id == 'bundle_types') {
            continue;
          }
          elseif (empty($vals[$bundle_type_id])) {
            unset($vals[$bundle_type_id]);
          }
          else {
            $vals[$bundle_type_id]['fields'] = array_filter($vals[$bundle_type_id]['fields']);
          }
        }
        $config->set($entity_type_id, $vals);
      }
    }
    $config->save();

    \Drupal::service('views.views_data')->clear();

    parent::submitForm($form, $form_state);
  }

}

