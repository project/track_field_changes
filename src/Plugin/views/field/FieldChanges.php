<?php

namespace Drupal\track_field_changes\Plugin\views\field;

use Drupal\Component\Utility\Html;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to provide field values.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("track_field_changes_value")
 */
class FieldChanges extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $output = '';

    if ($this->getValue($values, 'field_name') == 'log') {
      return '';
    }

    $value = $this->getValue($values);
    $fields = json_decode($value, TRUE);

    if (!empty($fields)) {
      $output = [];
      foreach ($fields as $delta => $field) {
        if ($delta === 'target_type') {
          continue;
        }
        foreach ($field as $key => $value) {
          if ($key == 'target_id' && !empty($fields['target_type']) && is_numeric($value)) {
            $entity = \Drupal::entityTypeManager()->getStorage($fields['target_type'])->load($value);
            if ($entity) {
              $output[] = $entity->label();
              continue;
            }
          }
          $output[] = Html::escape($value);
        }
      }

      $output = implode('  ', $output);
    }

    return $output;
  }

}
