<?php

namespace Drupal\track_field_changes\Plugin\views\field;

use Drupal\Component\Utility\Html;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to provide field values.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("track_field_changes_field")
 */
class FieldName extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    if ($this->getValue($values) == 'log') {
      return '';
    }
    if ($entity->get($this->getValue($values))) {
      $definition = $entity->get($this->getValue($values))->getDataDefinition();
      $output = $definition ? $definition->getLabel() : $entity->get($this->getValue($values))->getName();
    }
    else {
      $output = '';
    }

    return $output;
  }

}
