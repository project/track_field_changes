<?php

/**
 * Implements hook_views_data().
 */
function track_field_changes_views_data() {
  $data = [];

  $data['track_field_changes_audit']['table']['group'] = t('Field Change Tracker');

  $entity_types = \Drupal::config('track_field_changes.settings')->get();
  if (!empty($entity_types)) {
    $data['track_field_changes_audit']['table']['join'] = [];
    foreach ($entity_types as $entity_type => $bundles) {
      $storage = \Drupal::entityTypeManager()->getDefinition($entity_type);
      $data['track_field_changes_audit']['table']['join'][$storage->getDataTable()] = [
        'field' => 'entity_id',
        'left_field' => $storage->getKey('id'),
        'extra' => [['field' => 'entity_type', 'value' => $entity_type]],
      ];
    }
  }

  $data['track_field_changes_audit']['field_name'] = [
    'title' => t('Field Name'),
    'help' => t('The unique id key for the active domain.'),
    'field' => [
      'id' => 'track_field_changes_field',
      'additional fields' => ['entity_type']
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['track_field_changes_audit']['timestamp'] = [
    'title' => t('Creation Date'),
    'help' => t('Display the time the field was updated by a user.'),
    'field' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];

  $data['track_field_changes_audit']['before_value_text'] = [
    'title' => t('Value Before'),
    'help' => t('The value before the update.'),
    'field' => [
      'id' => 'track_field_changes_value',
      'additional fields' => ['field_name']
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['track_field_changes_audit']['after_value_text'] = [
    'title' => t('Value After'),
    'help' => t('The value after the update.'),
    'field' => [
      'id' => 'track_field_changes_value',
      'additional fields' => ['field_name']
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['track_field_changes_audit']['type'] = [
    'title' => t('Track Changes Type'),
    'help' => t('New or Revision'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['track_field_changes_audit']['log'] = [
    'title' => t('Track Changes Log'),
    'help' => t('The log message entered when the field was changed.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['track_field_changes_audit']['uid'] = [
    'title' => t('UID'),
    'help' => t('The user who updated the field.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'title' => t('User'),
      'help' => t('The user that made the field change.'),
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
    ],
  ];

  return $data;
}
